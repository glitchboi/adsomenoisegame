// (function(){

var transparent = false;
var antialias = true;

	var game = new Phaser.Game(960, 640, Phaser.AUTO, 'phaser-example', this, transparent, antialias);
	var states = {
		'Boot': ASN.Boot,
		'Preloader': ASN.Preloader,
		'MainMenu': ASN.MainMenu,
		'Achievements': ASN.Achievements,
		'Story': ASN.Story,
		'Game': ASN.Game
	};
	for(var state in states)
		game.state.add(state, states[state]);
	game.state.start('Boot');

// })();
