ASN.MainMenu = function(game) {};
var player;
	var email;
		var validatedEmail = false; 
		var validatedName = false;
ASN.MainMenu.prototype = {


	create: function() {
		// this.add.sprite(0, 0, 'background');
		var title = this.add.sprite(this.world.width*0.5, (this.world.height-100)*0.4, 'title');
		title.anchor.set(0.5);
		var validatedEmail = false; 
		var validatedName = false;

		ASN.Storage = this.game.plugins.add(Phaser.Plugin.Storage);

		ASN.Storage.initUnset('EPT-highscore', 0);
		var highscore = ASN.Storage.get('EPT-highscore') || 0;

		var buttonStart = this.add.button(this.world.width-20, this.world.height-20, 'button-start', this.clickStart, this, 1, 0, 2);
		buttonStart.anchor.set(1);

		this.buttonAudio = this.add.button(this.world.width-20, 20, 'button-audio', this.clickAudio, this, 1, 0, 2);
		this.buttonAudio.anchor.set(1,0);

		var buttonAchievements = this.add.button(20, this.world.height-20, 'button-achievements', this.clickAchievements, this, 1, 0, 2);
		buttonAchievements.anchor.set(0,1);

		var fontHighscore = { font: "32px Arial", fill: "#000" };
		var textHighscore = this.add.text(this.world.width*0.5, this.world.height-50, 'Highscore: '+highscore, fontHighscore);
		textHighscore.anchor.set(0.5,1);

		ASN._manageAudio('init',this);
		// Turn the music off at the start:
		ASN._manageAudio('off',this);

		buttonStart.x = this.world.width+buttonStart.width+20;
		this.add.tween(buttonStart).to({x: this.world.width-20}, 500, Phaser.Easing.Exponential.Out, true);
		this.buttonAudio.y = -this.buttonAudio.height-20;
		this.add.tween(this.buttonAudio).to({y: 20}, 500, Phaser.Easing.Exponential.Out, true);
		
		player = this.add.inputField(355,300, {
   			font: '18px Arial',
    		fill: '#212121',
    		fontWeight: 'bold',
    		width: 250,
   			padding: 8,
    		borderWidth: 1,
    		borderColor: '#000',
    		borderRadius: 6,
    		placeHolder: 'Name',
    		type: PhaserInput.InputType.text
		});

		email = this.add.inputField(355, 350, {
   			font: '18px Arial',
    		fill: '#212121',
    		fontWeight: 'bold',
    		width: 250,
   			padding: 8,
    		borderWidth: 1,
    		borderColor: '#000',
    		borderRadius: 6,
    		placeHolder: 'E-mail adres',
    		type: PhaserInput.InputType.text,
    		update: function () {
    		this._inputField.update();
		}
		});
		var validatedEmail = false; var validatedName = false;
		buttonAchievements.y = this.world.height+buttonAchievements.height+20;
		this.add.tween(buttonAchievements).to({y: this.world.height-20}, 500, Phaser.Easing.Exponential.Out, true);

		this.camera.flash(0x000000, 500, false);
	},
	clickAudio: function() {
		ASN._playAudio('click');
		ASN._manageAudio('switch',this);
	},
	
	clickStart: function() {
		ASN._playAudio('click');
		console.log("Clicked");
		validatedEmail = validateEmail(email.value);
 		validatedName = validateName(player.value);
		if(validatedEmail && validatedName){
			ASN.Storage.set('playerName', player.value);
			ASN.Storage.set('email', email.value);
			this.camera.fade(0x000000, 200, false);
			this.time.events.add(200, function() {
				this.game.state.start('Story');
			}, this);
		} else{
			//error
		}
	},
	clickAchievements: function() {
		ASN._playAudio('click');
		this.game.state.start('Achievements');
	}
};

function validateEmail(emailText){
var pattern = /^[a-zA-Z0-9\-_]+(\.[a-zA-Z0-9\-_]+)*@[a-z0-9]+(\-[a-z0-9]+)*(\.[a-z0-9]+(\-[a-z0-9]+)*)*\.[a-z]{2,4}$/;
    if (pattern.test(emailText)) {
        return true;
    } else {
    	game.add.text(355, 400, "Email input not valid", {
                    font: '18px Arial'
                })
    	    	 console.log("Email input not valid")

        return false;
    }
}

function validateName(name){
var pattern =  /^[a-zA-Z ]{2,30}$/;
    if (pattern.test(name)) {
        return true;
    } else {
    	game.add.text(355, 420, "Name input not valid", {
                    font: '18px Arial'
                });
    	 console.log("Name input not valid")
        return false;
    }
}