var ASN = {
	_manageAudio: function(mode, game) {
		switch(mode) {
			case 'init': {
				ASN.Storage.initUnset('EPT-audio', true);
				ASN._audioStatus = ASN.Storage.get('EPT-audio');
				// ASN._soundClick = game.add.audio('audio-click');
				ASN._sound = [];
				ASN._sound['click'] = game.add.audio('audio-click');
				if(!ASN._soundMusic) {
					ASN._soundMusic = game.add.audio('audio-theme',1,true);
					ASN._soundMusic.volume = 0.5;
				}
				break;
			}
			case 'on': {
				ASN._audioStatus = true;
				break;
			}
			case 'off': {
				ASN._audioStatus = false;
				break;
			}
			case 'switch': {
				ASN._audioStatus =! ASN._audioStatus;
				break;
			}
			default: {}
		}
		if(ASN._audioStatus) {
			ASN._audioOffset = 0;
			if(ASN._soundMusic) {
				if(!ASN._soundMusic.isPlaying) {
					ASN._soundMusic.play('',0,1,true);
				}
			}
		}
		else {
			ASN._audioOffset = 4;
			if(ASN._soundMusic) {
				ASN._soundMusic.stop();
			}
		}
		ASN.Storage.set('EPT-audio',ASN._audioStatus);
		game.buttonAudio.setFrames(ASN._audioOffset+1, ASN._audioOffset+0, ASN._audioOffset+2);
	},
	_playAudio: function(sound) {
		if(ASN._audioStatus) {
			if(ASN._sound && ASN._sound[sound]) {
				ASN._sound[sound].play();
			}
		}
	}
};
ASN.Boot = function(game){};
ASN.Boot.prototype = {
	preload: function(){
		this.stage.backgroundColor = '#F1F1F1';
		this.load.image('loading-background', 'img/loading-background.png');
		this.load.image('loading-progress', 'img/loading-progress.png');
	},
	create: function(){
		this.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
		this.scale.pageAlignHorizontally = true;
		this.scale.pageAlignVertically = true;
		this.state.start('Preloader');
		Phaser.Canvas.setSmoothingEnabled(this.game.canvas)  
		game.plugins.add(Phaser.Plugin.Inspector);

	}
};